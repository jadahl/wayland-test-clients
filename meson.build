project('wayland-test-clients', 'c',
        version: '0.0.1',
        default_options: [
          'warning_level=1',
          'c_std=gnu99',
          'buildtype=debug'
        ]
)

cc = meson.get_compiler('c')
add_global_arguments('-D_GNU_SOURCE', language : 'c')

if not cc.has_header_symbol('stdlib.h', 'mkostemp',
                            args: ['-D_GNU_SOURCE'])
  error('No mkostemp')
endif
if not cc.has_header_symbol('fcntl.h', 'posix_fallocate')
  error('No posix_fallocate')
endif

glib_dep = dependency('glib-2.0')
wayland_client_dep = dependency('wayland-client')
m_dep = cc.find_library('m', required: false)

# wayland protocols
proto_dir = dependency('wayland-protocols').get_pkgconfig_variable('pkgdatadir')
assert(proto_dir != '', 'Could not get pkgdatadir from wayland-protocols.pc')

wayland_scanner = find_program('wayland-scanner')

# Format:
#  - protocol name
#  - protocol stability ('private', 'stable' or 'unstable')
#  - protocol version (if stability is 'unstable')
proto_sources = [
  ['xdg-shell', 'stable', ],
  ['viewporter', 'stable', ],
  ['gtk-shell', 'private', ],
]

wayland_gen_headers = []
wayland_gen_sources = []

foreach p: proto_sources
  proto_name = p.get(0)
  proto_stability = p.get(1)

  if proto_stability == 'stable'
    output_base = proto_name
    input = join_paths(proto_dir, '@0@/@1@/@2@.xml'.format(proto_stability,
                                                           proto_name,
                                                            output_base))
  elif proto_stability == 'private'
    output_base = proto_name
    input = '@0@.xml'.format(proto_name)
  else
    proto_version = p.get(2)
    output_base = '@0@-@1@-@2@'.format(proto_name, proto_stability, proto_version)
    input = join_paths(proto_dir, '@0@/@1@/@2@.xml'.format(proto_stability,
                                                           proto_name,
                                                           output_base))
  endif

  wayland_gen_headers += custom_target('@0@ client header'.format(output_base),
                                       input: input,
                                       output: '@0@-client-protocol.h'.format(output_base),
                                       command: [
                                         wayland_scanner,
                                         'client-header',
                                         '@INPUT@', '@OUTPUT@',
                                       ]
  )

  wayland_gen_sources += custom_target('@0@ source'.format(output_base),
                                       input: input,
                                       output: '@0@-protocol.c'.format(output_base),
                                       command: [
                                         wayland_scanner,
                                         'private-code',
                                         '@INPUT@', '@OUTPUT@',
                                       ])
endforeach

common_sources = [
  'utils.c',
  'utils.h',
  wayland_gen_sources,
  wayland_gen_headers,
]

executable('gradient-test',
           sources: [
             common_sources,
             'half.c',
             'half.h',
             'gradient-test.c',
           ],
           dependencies: [
             glib_dep,
             wayland_client_dep,
             m_dep,
           ],
)

executable('xdg-shell',
           sources: [
             common_sources,
             'xdg-shell.c',
           ],
           dependencies: [
             glib_dep,
             wayland_client_dep,
           ],
)

executable('viewport',
           sources: [
             common_sources,
             'viewport.c',
           ],
           dependencies: [
             glib_dep,
             wayland_client_dep,
           ],
)

executable('dialogs',
           sources: [
             common_sources,
             'dialogs.c',
           ],
           dependencies: [
             glib_dep,
             wayland_client_dep,
           ],
)
