#include <glib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client.h>

#include "utils.h"

#include "viewporter-client-protocol.h"
#include "xdg-shell-client-protocol.h"

static const int default_width = 100;
static const int default_height = 100;

static struct wl_display *display;
static struct wl_registry *registry;
static struct wl_compositor *compositor;
static struct xdg_wm_base *xdg_wm_base;
static struct wl_shm *shm;
static struct wp_viewporter *viewporter;

static struct wl_surface *surface;
static struct xdg_surface *xdg_surface;
static struct xdg_toplevel *xdg_toplevel;
static struct wp_viewport *viewport;

static int configured_width;
static int configured_height;

static gboolean fullscreen_full_size;
static gboolean wait_for_configure;
static gboolean running;

static void
handle_buffer_release (void             *data,
                       struct wl_buffer *buffer)
{
  wl_buffer_destroy (buffer);
}

static const struct wl_buffer_listener buffer_listener = {
  handle_buffer_release
};

static gboolean
create_shm_buffer (int                width,
                   int                height,
                   struct wl_buffer **out_buffer,
                   void             **out_data,
                   int               *out_size)
{
  struct wl_shm_pool *pool;
  static struct wl_buffer *buffer;
  int fd, size, stride;
  int bytes_per_pixel;
  void *data;

  bytes_per_pixel = 4;
  stride = width * bytes_per_pixel;
  size = stride * height;

  fd = create_anonymous_file (size);
  if (fd < 0)
    {
      fprintf (stderr, "Creating a buffer file for %d B failed: %m\n",
               size);
      return FALSE;
    }

  data = mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (data == MAP_FAILED)
    {
      fprintf (stderr, "mmap failed: %m\n");
      close (fd);
      return FALSE;
    }

  pool = wl_shm_create_pool (shm, fd, size);
  buffer = wl_shm_pool_create_buffer (pool, 0,
                                      width, height,
                                      stride,
                                      WL_SHM_FORMAT_ARGB8888);
  wl_buffer_add_listener (buffer, &buffer_listener, buffer);
  wl_shm_pool_destroy (pool);
  close (fd);

  *out_buffer = buffer;
  *out_data = data;
  *out_size = size;

  return TRUE;
}

static void
fill (void    *buffer_data,
      int      width,
      int      height,
      uint32_t color)
{
  uint32_t *pixels = buffer_data;
  int x, y;

  for (y = 0; y < height; y++)
    {
      for (x = 0; x < width; x++)
        pixels[y * width + x] = color;
    }
}

static void
draw (void)
{
  struct wl_buffer *buffer;
  void *buffer_data;
  int width;
  int height;
  int size;
  int buffer_width;
  int buffer_height;
  int buffer_scale;

  if (configured_width == 0)
    width = default_width;
  else
    width = configured_width;
  if (configured_height == 0)
    height = default_height;
  else
    height = configured_height;

#if 2
  buffer_width = 400;
  buffer_height = 400;
  buffer_scale = 1;
#else
  buffer_width = 800;
  buffer_height = 800;
  buffer_scale = 2;
#endif

  if (!create_shm_buffer (buffer_width, buffer_height,
                          &buffer, &buffer_data, &size))
    g_error ("Failed to create shm buffer");

  fill (buffer_data, buffer_width, buffer_height,
        0xff00ff00);

  munmap (buffer_data, size);

  xdg_surface_set_window_geometry (xdg_surface, 0, 0, width, height);

  if (!viewport)
    viewport = wp_viewporter_get_viewport (viewporter, surface);
  wp_viewport_set_source (viewport,
                          wl_fixed_from_int (150),
                          wl_fixed_from_int (150),
                          wl_fixed_from_int (100),
                          wl_fixed_from_int (100));
  wp_viewport_set_destination (viewport, width, height);

  wl_surface_set_buffer_scale (surface, buffer_scale);
  wl_surface_attach (surface, buffer, 0, 0);
}

typedef enum _ToplevelState
{
  TOPLEVEL_STATE_NONE = 0,
  TOPLEVEL_STATE_FULLSCREEN = 1 << 0,
} ToplevelState;

static ToplevelState
get_toplevel_state (struct wl_array *state_array)
{
  uint32_t *state_value;
  ToplevelState state = TOPLEVEL_STATE_NONE;

  wl_array_for_each(state_value, state_array)
    {
      switch ((enum xdg_toplevel_state) *state_value)
        {
        case XDG_TOPLEVEL_STATE_FULLSCREEN:
          state |= TOPLEVEL_STATE_FULLSCREEN;
          break;
        case XDG_TOPLEVEL_STATE_MAXIMIZED:
        case XDG_TOPLEVEL_STATE_TILED_LEFT:
        case XDG_TOPLEVEL_STATE_TILED_RIGHT:
        case XDG_TOPLEVEL_STATE_TILED_TOP:
        case XDG_TOPLEVEL_STATE_TILED_BOTTOM:
          break;
        case XDG_TOPLEVEL_STATE_RESIZING:
        case XDG_TOPLEVEL_STATE_ACTIVATED:
          break;
        }
    }

  return state;
}

static void
handle_xdg_toplevel_configure (void                *data,
                               struct xdg_toplevel *xdg_toplevel,
                               int32_t              width,
                               int32_t              height,
                               struct wl_array     *state_array)
{
  ToplevelState state;

  state = get_toplevel_state (state_array);

  if (state & TOPLEVEL_STATE_FULLSCREEN &&
      !fullscreen_full_size)
    {
      configured_width = default_width;
      configured_height = default_height;
    }
  else
    {
      configured_width = width;
      configured_height = height;
    }

  draw ();
}

static void
handle_xdg_toplevel_close(void                *data,
                          struct xdg_toplevel *xdg_toplevel)
{
  running = FALSE;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
  handle_xdg_toplevel_configure,
  handle_xdg_toplevel_close,
};

static void
handle_xdg_surface_configure (void               *data,
                              struct xdg_surface *xdg_surface,
                              uint32_t            serial)
{
  if (wait_for_configure)
    {
      draw ();
      wait_for_configure = FALSE;
    }

  xdg_surface_ack_configure (xdg_surface, serial);
  wl_surface_commit (surface);
}

static const struct xdg_surface_listener xdg_surface_listener = {
  handle_xdg_surface_configure,
};

static void
handle_xdg_wm_base_ping (void               *data,
                         struct xdg_wm_base *xdg_wm_base,
                         uint32_t            serial)
{
  xdg_wm_base_pong (xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
  handle_xdg_wm_base_ping,
};

static void
handle_registry_global (void               *data,
                        struct wl_registry *registry,
                        uint32_t            id,
                        const char         *interface,
                        uint32_t            version)
{
  if (strcmp (interface, "wl_compositor") == 0)
    {
      compositor = wl_registry_bind (registry, id, &wl_compositor_interface, 3);
    }
  else if (strcmp (interface, "xdg_wm_base") == 0)
    {
      xdg_wm_base = wl_registry_bind (registry, id,
                                      &xdg_wm_base_interface, 1);
      xdg_wm_base_add_listener (xdg_wm_base, &xdg_wm_base_listener, NULL);
    }
  else if (strcmp (interface, "wl_shm") == 0)
    {
      shm = wl_registry_bind (registry,
                              id, &wl_shm_interface, 1);
    }
  else if (strcmp (interface, "wp_viewporter") == 0)
    {
      viewporter = wl_registry_bind (registry,
                                     id, &wp_viewporter_interface, 1);
    }
}

static void
handle_registry_global_remove (void               *data,
                               struct wl_registry *registry,
                               uint32_t            name)
{
}

static const struct wl_registry_listener registry_listener = {
  handle_registry_global,
  handle_registry_global_remove
};

static void
init_surface (void)
{
  xdg_toplevel_set_title (xdg_toplevel, "viewport-test");
  wl_surface_commit (surface);

  wait_for_configure = TRUE;
}

int
main (int    argc,
      char **argv)
{
  display = wl_display_connect (NULL);
  registry = wl_display_get_registry (display);
  wl_registry_add_listener (registry, &registry_listener, NULL);
  wl_display_roundtrip (display);

  if (!shm)
    {
      fprintf (stderr, "No wl_shm global\n");
      return EXIT_FAILURE;
    }

  if (!xdg_wm_base)
    {
      fprintf (stderr, "No xdg_wm_base global\n");
      return EXIT_FAILURE;
    }

  wl_display_roundtrip (display);

  surface = wl_compositor_create_surface (compositor);
  xdg_surface = xdg_wm_base_get_xdg_surface (xdg_wm_base, surface);
  xdg_surface_add_listener (xdg_surface, &xdg_surface_listener, NULL);
  xdg_toplevel = xdg_surface_get_toplevel (xdg_surface);
  xdg_toplevel_add_listener (xdg_toplevel, &xdg_toplevel_listener, NULL);

  init_surface ();

  running = TRUE;
  while (running)
    {
      if (wl_display_dispatch (display) == -1)
        return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
