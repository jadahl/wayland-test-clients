#ifndef HALF_H
#define HALF_H
#include <stdint.h>

uint16_t half_from_float (uint32_t f);

uint32_t half_to_float (uint16_t h);

uint16_t half_add (uint16_t x,
                   uint16_t y);

uint16_t half_mul (uint16_t x,
                   uint16_t y);

#endif /* HALF_H */
