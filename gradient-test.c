/*
 * Copyright © 2018-2019 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <glib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client.h>

#include "half.h"
#include "utils.h"

#include "xdg-shell-client-protocol.h"

static_assert (G_BYTE_ORDER == G_LITTLE_ENDIAN, "only little-endian is supported");

typedef enum _PixelFormat
{
  PIXEL_FORMAT_RGB5,
  PIXEL_FORMAT_ARGB8,
  PIXEL_FORMAT_XRGB8,
  PIXEL_FORMAT_ARGB10,
  PIXEL_FORMAT_XRGB10,
  PIXEL_FORMAT_ABGR10,
  PIXEL_FORMAT_XBGR10,
  PIXEL_FORMAT_ARGB16,
  PIXEL_FORMAT_XRGB16,
  PIXEL_FORMAT_ABGR16,
  PIXEL_FORMAT_XBGR16,
} PixelFormat;

typedef struct _Color
{
  float r;
  float g;
  float b;
  float a;
} Color;

static const struct {
  PixelFormat pixel_format;
  uint32_t shm_pixel_format;
  const char *pixel_format_string;
} supported_pixel_formats[] = {
  { PIXEL_FORMAT_RGB5, WL_SHM_FORMAT_RGB565, "rgb5" },
  { PIXEL_FORMAT_ARGB8, WL_SHM_FORMAT_ARGB8888, "argb8" },
  { PIXEL_FORMAT_XRGB8, WL_SHM_FORMAT_XRGB8888, "xrgb8" },
  { PIXEL_FORMAT_ARGB10, WL_SHM_FORMAT_ARGB2101010, "argb10" },
  { PIXEL_FORMAT_XRGB10, WL_SHM_FORMAT_XRGB2101010, "xrgb10" },
  { PIXEL_FORMAT_ABGR10, WL_SHM_FORMAT_ABGR2101010, "abgr10" },
  { PIXEL_FORMAT_XBGR10, WL_SHM_FORMAT_XBGR2101010, "xbgr10" },
  { PIXEL_FORMAT_ARGB16, WL_SHM_FORMAT_ARGB16161616F, "argb16" },
  { PIXEL_FORMAT_XRGB16, WL_SHM_FORMAT_XRGB16161616F, "xrgb16" },
  { PIXEL_FORMAT_ABGR16, WL_SHM_FORMAT_ABGR16161616F, "abgr16" },
  { PIXEL_FORMAT_XBGR16, WL_SHM_FORMAT_XBGR16161616F, "xbgr16" },
};

static struct wl_display *display;
static struct wl_registry *registry;
static struct wl_compositor *compositor;
static struct xdg_wm_base *xdg_wm_base;
static struct wl_shm *shm;

static GList *shm_formats;

static struct wl_surface *surface;
static struct xdg_surface *xdg_surface;
static struct xdg_toplevel *xdg_toplevel;

static PixelFormat pixel_format;
static uint32_t shm_pixel_format;

static Color left_color;
static Color right_color;

static gboolean wait_for_configure; 

static gboolean running;

static void
handle_buffer_release (void             *data,
                       struct wl_buffer *buffer)
{
  wl_buffer_destroy (buffer);
}

static const struct wl_buffer_listener buffer_listener = {
  handle_buffer_release
};

static int
bytes_per_pixel_from_format (PixelFormat format)
{
  switch (format)
    {
    case PIXEL_FORMAT_RGB5:
      return 2;
    case PIXEL_FORMAT_ARGB8:
    case PIXEL_FORMAT_XRGB8:
    case PIXEL_FORMAT_ARGB10:
    case PIXEL_FORMAT_XRGB10:
    case PIXEL_FORMAT_ABGR10:
    case PIXEL_FORMAT_XBGR10:
      return 4;
    case PIXEL_FORMAT_ARGB16:
    case PIXEL_FORMAT_XRGB16:
    case PIXEL_FORMAT_ABGR16:
    case PIXEL_FORMAT_XBGR16:
      return 8;
    }

  g_assert_not_reached ();
}

static gboolean
create_shm_buffer (int                width,
                   int                height,
                   PixelFormat        pixel_format,
                   uint32_t           shm_pixel_format,
                   struct wl_buffer **out_buffer,
                   void             **out_data,
                   int               *out_size)
{
  struct wl_shm_pool *pool;
  static struct wl_buffer *buffer;
  int fd, size, stride;
  int bytes_per_pixel;
  void *data;

  bytes_per_pixel = bytes_per_pixel_from_format (pixel_format);
  stride = width * bytes_per_pixel;
  size = stride * height;

  fd = create_anonymous_file (size);
  if (fd < 0)
    {
      fprintf (stderr, "Creating a buffer file for %d B failed: %m\n",
               size);
      return FALSE;
    }

  data = mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (data == MAP_FAILED)
    {
      fprintf (stderr, "mmap failed: %m\n");
      close (fd);
      return FALSE;
    }

  pool = wl_shm_create_pool (shm, fd, size);
  buffer = wl_shm_pool_create_buffer (pool, 0,
                                      width, height,
                                      stride, shm_pixel_format);
  wl_buffer_add_listener (buffer, &buffer_listener, buffer);
  wl_shm_pool_destroy (pool);
  close (fd);

  *out_buffer = buffer;
  *out_data = data;
  *out_size = size;

  return TRUE;
}

static Color
current_gradient_color (int   x,
                        int   width,
                        Color left,
                        Color right)
{
  float ratio;

  ratio = (float) x / (float) width;

  return (Color) {
    .r = left.r + (right.r - left.r) * ratio,
    .g = left.g + (right.g - left.g) * ratio,
    .b = left.b + (right.b - left.b) * ratio,
    .a = left.a + (right.a - left.a) * ratio,
  };
}

static void
put_pixel_rgb5 (void  *data,
                int    x,
                int    y,
                int    width,
                Color  color)
{
  uint16_t *pixels = data;
  uint8_t r, g, b;
  uint16_t pixel;

  r = (uint8_t) roundf (color.r * 0x1f);
  g = (uint8_t) roundf (color.g * 0x3f);
  b = (uint8_t) roundf (color.b * 0x1f);
  pixel = (r << 11 |
           g << 5 |
           b);

  pixels[x + y * width] = pixel;
}

static void
put_pixel_argb8 (void  *data,
                 int    x,
                 int    y,
                 int    width,
                 Color  color)
{
  uint32_t *pixels = data;
  uint8_t r, g, b, a;
  uint32_t pixel;

  r = (uint8_t) roundf (color.r * 0xff);
  g = (uint8_t) roundf (color.g * 0xff);
  b = (uint8_t) roundf (color.b * 0xff);
  a = (uint8_t) roundf (color.a * 0xff);
  pixel = (a << 24 |
           r << 16 |
           g << 8 |
           b);

  pixels[x + y * width] = pixel;
}

static void
put_pixel_argb10 (void  *data,
                  int    x,
                  int    y,
                  int    width,
                  Color  color)
{
  uint32_t *pixels = data;
  uint16_t r, g, b, a;
  uint32_t pixel;

  r = (uint16_t) roundf (color.r * 0x3ff);
  g = (uint16_t) roundf (color.g * 0x3ff);
  b = (uint16_t) roundf (color.b * 0x3ff);
  a = (uint16_t) roundf (color.a * 0x3);
  pixel = (a << 30 |
           r << 20 |
           g << 10 |
           b);

  pixels[x + y * width] = pixel;
}

static void
put_pixel_abgr10 (void  *data,
                  int    x,
                  int    y,
                  int    width,
                  Color  color)
{
  uint32_t *pixels = data;
  uint16_t r, g, b, a;
  uint32_t pixel;

  r = (uint16_t) roundf (color.r * 0x3ff);
  g = (uint16_t) roundf (color.g * 0x3ff);
  b = (uint16_t) roundf (color.b * 0x3ff);
  a = (uint16_t) roundf (color.a * 0x3);
  pixel = (a << 30 |
           b << 20 |
           g << 10 |
           r);

  pixels[x + y * width] = pixel;
}

static void
put_pixel_argb16 (void  *data,
                  int    x,
                  int    y,
                  int    width,
                  Color  color)
{
  uint64_t *pixels = data;
  uint64_t r, g, b, a;
  uint64_t pixel;

  r = half_from_float (*((uint32_t *) &color.r));
  g = half_from_float (*((uint32_t *) &color.g));
  b = half_from_float (*((uint32_t *) &color.b));
  a = half_from_float (*((uint32_t *) &color.a));
  pixel = (a << 48 |
           r << 32 |
           g << 16 |
           b);

  pixels[x + y * width] = pixel;
}

static void
put_pixel_abgr16 (void  *data,
                  int    x,
                  int    y,
                  int    width,
                  Color  color)
{
  uint64_t *pixels = data;
  uint64_t r, g, b, a;
  uint64_t pixel;

  r = half_from_float (*((uint32_t *) &color.r));
  g = half_from_float (*((uint32_t *) &color.g));
  b = half_from_float (*((uint32_t *) &color.b));
  a = half_from_float (*((uint32_t *) &color.a));
  pixel = (a << 48 |
           b << 32 |
           g << 16 |
           r);

  pixels[x + y * width] = pixel;
}

static void
put_pixel (void        *data,
           PixelFormat  format,
           int          x,
           int          y,
           int          width,
           Color        color)
{
  switch (format)
    {
    case PIXEL_FORMAT_RGB5:
      put_pixel_rgb5 (data, x, y, width, color);
      break;
    case PIXEL_FORMAT_ARGB8:
    case PIXEL_FORMAT_XRGB8:
      put_pixel_argb8 (data, x, y, width, color);
      break;
    case PIXEL_FORMAT_ARGB10:
    case PIXEL_FORMAT_XRGB10:
      put_pixel_argb10 (data, x, y, width, color);
      break;
    case PIXEL_FORMAT_ABGR10:
    case PIXEL_FORMAT_XBGR10:
      put_pixel_abgr10 (data, x, y, width, color);
      break;
    case PIXEL_FORMAT_ARGB16:
    case PIXEL_FORMAT_XRGB16:
      put_pixel_argb16 (data, x, y, width, color);
      break;
    case PIXEL_FORMAT_ABGR16:
    case PIXEL_FORMAT_XBGR16:
      put_pixel_abgr16 (data, x, y, width, color);
      break;
    }
}

static void
fill_gradient (Color        left,
               Color        right,
               int          width,
               int          height,
               PixelFormat  format,
               void        *data)
{
  int x;

  for (x = 0; x < width; x++)
    {
      Color color;
      int y;

      color = current_gradient_color (x, width, left, right);

      for (y = 0; y < height; y++)
        put_pixel (data, format, x, y, width, color);
    }
}

static void
draw (void)
{
  struct wl_buffer *buffer;
  void *buffer_data;
  const int width = 900;
  const int height = 900;
  int size;

  if (!create_shm_buffer (width, height,
                          pixel_format, shm_pixel_format,
                          &buffer, &buffer_data, &size))
    g_error ("Failed to create shm buffer");

  fill_gradient (left_color,
                 right_color,
                 width,
                 height,
                 pixel_format,
                 buffer_data);

  wl_surface_attach (surface, buffer, 0, 0);
}

static void
handle_xdg_toplevel_configure (void                *data,
                               struct xdg_toplevel *xdg_toplevel,
                               int32_t              width,
                               int32_t              height,
                               struct wl_array     *state)
{
}

static void
handle_xdg_toplevel_close(void                *data,
                          struct xdg_toplevel *xdg_toplevel)
{
  running = FALSE;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
  handle_xdg_toplevel_configure,
  handle_xdg_toplevel_close,
};

static void
handle_xdg_surface_configure (void               *data,
                              struct xdg_surface *xdg_surface,
                              uint32_t            serial)
{
  if (wait_for_configure)
    {
      draw ();
      wait_for_configure = FALSE;
    }

  xdg_surface_ack_configure (xdg_surface, serial);
  wl_surface_commit (surface);
}

static const struct xdg_surface_listener xdg_surface_listener = {
  handle_xdg_surface_configure,
};

static void
handle_xdg_wm_base_ping (void               *data,
                         struct xdg_wm_base *xdg_wm_base,
                         uint32_t            serial)
{
  xdg_wm_base_pong (xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
  handle_xdg_wm_base_ping,
};

static void
handle_shm_format (void          *data,
                   struct wl_shm *wl_shm,
                   uint32_t       format)
{
  shm_formats = g_list_prepend (shm_formats, GINT_TO_POINTER (format));
}

struct wl_shm_listener shm_listener = {
  handle_shm_format
};

static void
handle_registry_global (void               *data,
                        struct wl_registry *registry,
                        uint32_t            id,
                        const char         *interface,
                        uint32_t            version)
{
  if (strcmp (interface, "wl_compositor") == 0)
    {
      compositor = wl_registry_bind (registry, id, &wl_compositor_interface, 1);
    }
  else if (strcmp (interface, "xdg_wm_base") == 0)
    {
      xdg_wm_base = wl_registry_bind (registry, id,
                                      &xdg_wm_base_interface, 1);
      xdg_wm_base_add_listener (xdg_wm_base, &xdg_wm_base_listener, NULL);
    }
  else if (strcmp (interface, "wl_shm") == 0)
    {
      shm = wl_registry_bind (registry,
                              id, &wl_shm_interface, 1);
      wl_shm_add_listener (shm, &shm_listener, NULL);
    }
}

static void
handle_registry_global_remove (void               *data,
                               struct wl_registry *registry,
                               uint32_t            name)
{
}

static const struct wl_registry_listener registry_listener = {
  handle_registry_global,
  handle_registry_global_remove
};

static uint32_t
shm_format_from_pixel_format (PixelFormat pixel_format)
{
  int i;

  for (i = 0; i < G_N_ELEMENTS (supported_pixel_formats); i++)
    {
      if (supported_pixel_formats[i].pixel_format == pixel_format)
        return supported_pixel_formats[i].shm_pixel_format;
    }

  g_assert_not_reached ();
}

static gboolean
determine_pixel_format (char        *pixel_format_string,
                        PixelFormat *out_pixel_format)
{
  int i;

  for (i = 0; i < G_N_ELEMENTS (supported_pixel_formats); i++)
    {
      if (strcmp (pixel_format_string,
                  supported_pixel_formats[i].pixel_format_string) == 0)
        {
          *out_pixel_format = supported_pixel_formats[i].pixel_format;
          return TRUE;
        }
    }

  return FALSE;
}

static void
list_pixel_formats (void)
{
  int i;

  for (i = 0; i < G_N_ELEMENTS (supported_pixel_formats); i++)
    fprintf (stdout, "%s\n", supported_pixel_formats[i].pixel_format_string);
}

static void
print_help (char **argv)
{
  fprintf (stdout, "Usage: %s [OPTIONS] [PIXEL-FORMAT] [COLOR]\n", argv[0]);
  fprintf (stdout, "Options:\n");
  fprintf (stdout, "    --list-pixel-formats      - List supported pixel formats.\n");
  fprintf (stdout, "    --help                    - Show help.\n");
}

int
main (int    argc,
      char **argv)
{
  char *pixel_format_string;

  if (argc == 2)
    {
      if (strcmp (argv[1], "--help") == 0)
        {
          print_help (argv);
          return EXIT_SUCCESS;
        }

      if (strcmp (argv[1], "--list-pixel-formats") == 0)
        {
          list_pixel_formats ();
          return EXIT_SUCCESS;
        }
    }
  else if (argc == 3)
    {
      pixel_format_string = argv[1];
      if (!determine_pixel_format (pixel_format_string, &pixel_format))
        {
          fprintf (stderr, "Unknown format %s\n", pixel_format_string);
          return EXIT_FAILURE;
        }

      if (strcmp (argv[2], "red") == 0)
        {
          left_color = (Color) { .r = 0.25, .a = 1.0 };
          right_color = (Color) { .r = 0.35, .a = 1.0 };
        }
      else if (strcmp (argv[2], "green") == 0)
        {
          left_color = (Color) { .g = 0.25, .a = 1.0 };
          right_color = (Color) { .g = 0.35, .a = 1.0 };
        }
      else if (strcmp (argv[2], "blue") == 0)
        {
          left_color = (Color) { .b = 0.25, .a = 1.0 };
          right_color = (Color) { .b = 0.35, .a = 1.0 };
        }
      else
        {
          fprintf (stderr, "Unknown color \"%s\", "
                   "(available colors: red, green and blue)\n",
                   argv[2]);
          return EXIT_FAILURE;
        }
    }
  else
    {
      print_help (argv);
      return EXIT_FAILURE;
    }

  shm_pixel_format = shm_format_from_pixel_format (pixel_format);

  display = wl_display_connect (NULL);
  registry = wl_display_get_registry (display);
  wl_registry_add_listener (registry, &registry_listener, NULL);
  wl_display_roundtrip (display);

  if (!shm)
    {
      fprintf (stderr, "No wl_shm global\n");
      return EXIT_FAILURE;
    }

  if (!xdg_wm_base)
    {
      fprintf (stderr, "No xdg_wm_base global\n");
      return EXIT_FAILURE;
    }

  wl_display_roundtrip (display);

  if (!g_list_find (shm_formats, GINT_TO_POINTER (shm_pixel_format)))
    {
      fprintf (stderr, "No server support for %s\n", pixel_format_string);
      return EXIT_FAILURE;
    }

  surface = wl_compositor_create_surface (compositor);
  xdg_surface = xdg_wm_base_get_xdg_surface (xdg_wm_base, surface);
  xdg_surface_add_listener (xdg_surface, &xdg_surface_listener, NULL);
  xdg_toplevel = xdg_surface_get_toplevel (xdg_surface);
  xdg_toplevel_add_listener (xdg_toplevel, &xdg_toplevel_listener, NULL);
  xdg_toplevel_set_title (xdg_toplevel, "gradient-test");
  wl_surface_commit (surface);

  wait_for_configure = TRUE;

  running = TRUE;
  while (running)
    {
      if (wl_display_dispatch (display) == -1)
        return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
