#include <glib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client.h>
#include <linux/input-event-codes.h>

#include "utils.h"

#include "xdg-shell-client-protocol.h"

static const int default_width = 700;
static const int default_height = 500;

static struct wl_display *display;
static struct wl_registry *registry;
static struct wl_compositor *compositor;
static struct xdg_wm_base *xdg_wm_base;
static struct wl_shm *shm;
static struct wl_seat *seat;
static struct wl_keyboard *keyboard;

static struct wl_surface *surface;
static struct xdg_surface *xdg_surface;
static struct xdg_toplevel *xdg_toplevel;

static gboolean fullscreen_requested;
static gboolean fullscreen_full_size;
static int configured_width;
static int configured_height;

static gboolean wait_for_configure;
static gboolean running;

static void
reset_surface (void);

static void
handle_buffer_release (void             *data,
                       struct wl_buffer *buffer)
{
  wl_buffer_destroy (buffer);
}

static const struct wl_buffer_listener buffer_listener = {
  handle_buffer_release
};

static gboolean
create_shm_buffer (int                width,
                   int                height,
                   struct wl_buffer **out_buffer,
                   void             **out_data,
                   int               *out_size)
{
  struct wl_shm_pool *pool;
  static struct wl_buffer *buffer;
  int fd, size, stride;
  int bytes_per_pixel;
  void *data;

  bytes_per_pixel = 4;
  stride = width * bytes_per_pixel;
  size = stride * height;

  fd = create_anonymous_file (size);
  if (fd < 0)
    {
      fprintf (stderr, "Creating a buffer file for %d B failed: %m\n",
               size);
      return FALSE;
    }

  data = mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (data == MAP_FAILED)
    {
      fprintf (stderr, "mmap failed: %m\n");
      close (fd);
      return FALSE;
    }

  pool = wl_shm_create_pool (shm, fd, size);
  buffer = wl_shm_pool_create_buffer (pool, 0,
                                      width, height,
                                      stride,
                                      WL_SHM_FORMAT_ARGB8888);
  wl_buffer_add_listener (buffer, &buffer_listener, buffer);
  wl_shm_pool_destroy (pool);
  close (fd);

  *out_buffer = buffer;
  *out_data = data;
  *out_size = size;

  return TRUE;
}

static void
fill (void    *buffer_data,
      int      width,
      int      height,
      uint32_t color)
{
  uint32_t *pixels = buffer_data;
  int x, y;

  for (y = 0; y < height; y++)
    {
      for (x = 0; x < width; x++)
        pixels[y * width + x] = color;
    }
}

static void
draw (void)
{
  struct wl_buffer *buffer;
  void *buffer_data;
  int width;
  int height;
  int size;

  if (configured_width == 0)
    width = default_width;
  else
    width = configured_width;
  if (configured_height == 0)
    height = default_height;
  else
    height = configured_height;

  if (!create_shm_buffer (width, height,
                          &buffer, &buffer_data, &size))
    g_error ("Failed to create shm buffer");

  fill (buffer_data, width, height,
        0xff00ff00);

  munmap (buffer_data, size);

  wl_surface_attach (surface, buffer, 0, 0);
}

static void
toggle_fullscreen_surface (gboolean full_size)
{
  if (fullscreen_requested)
    {
      xdg_toplevel_unset_fullscreen (xdg_toplevel);
      fullscreen_requested = FALSE;
    }
  else
    {
      xdg_toplevel_set_fullscreen (xdg_toplevel, NULL);
      fullscreen_requested = TRUE;
      fullscreen_full_size = full_size;
    }
}

typedef enum _ToplevelState
{
  TOPLEVEL_STATE_NONE = 0,
  TOPLEVEL_STATE_FULLSCREEN = 1 << 0,
} ToplevelState;

static ToplevelState
get_toplevel_state (struct wl_array *state_array)
{
  uint32_t *state_value;
  ToplevelState state = TOPLEVEL_STATE_NONE;

  wl_array_for_each(state_value, state_array)
    {
      switch ((enum xdg_toplevel_state) *state_value)
        {
        case XDG_TOPLEVEL_STATE_FULLSCREEN:
          state |= TOPLEVEL_STATE_FULLSCREEN;
          break;
        case XDG_TOPLEVEL_STATE_MAXIMIZED:
        case XDG_TOPLEVEL_STATE_TILED_LEFT:
        case XDG_TOPLEVEL_STATE_TILED_RIGHT:
        case XDG_TOPLEVEL_STATE_TILED_TOP:
        case XDG_TOPLEVEL_STATE_TILED_BOTTOM:
          break;
        case XDG_TOPLEVEL_STATE_RESIZING:
        case XDG_TOPLEVEL_STATE_ACTIVATED:
          break;
        }
    }

  return state;
}

static void
handle_xdg_toplevel_configure (void                *data,
                               struct xdg_toplevel *xdg_toplevel,
                               int32_t              width,
                               int32_t              height,
                               struct wl_array     *state_array)
{
  ToplevelState state;

  state = get_toplevel_state (state_array);

  if (state & TOPLEVEL_STATE_FULLSCREEN &&
      !fullscreen_full_size)
    {
      configured_width = default_width;
      configured_height = default_height;
    }
  else
    {
      configured_width = width;
      configured_height = height;
    }

  draw ();
}

static void
handle_xdg_toplevel_close(void                *data,
                          struct xdg_toplevel *xdg_toplevel)
{
  running = FALSE;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
  handle_xdg_toplevel_configure,
  handle_xdg_toplevel_close,
};

static void
handle_xdg_surface_configure (void               *data,
                              struct xdg_surface *xdg_surface,
                              uint32_t            serial)
{
  if (wait_for_configure)
    {
      draw ();
      wait_for_configure = FALSE;
    }

  xdg_surface_ack_configure (xdg_surface, serial);
  wl_surface_commit (surface);
}

static const struct xdg_surface_listener xdg_surface_listener = {
  handle_xdg_surface_configure,
};

static void
handle_xdg_wm_base_ping (void               *data,
                         struct xdg_wm_base *xdg_wm_base,
                         uint32_t            serial)
{
  xdg_wm_base_pong (xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
  handle_xdg_wm_base_ping,
};

static void
handle_keyboard_keymap (void *data,
                        struct wl_keyboard *wl_keyboard,
                        uint32_t format,
                        int32_t fd,
                        uint32_t size)
{
}

static void
handle_keyboard_enter (void *data,
                       struct wl_keyboard *wl_keyboard,
                       uint32_t serial,
                       struct wl_surface *surface,
                       struct wl_array *keys)
{
}

static void
handle_keyboard_leave (void *data,
                       struct wl_keyboard *wl_keyboard,
                       uint32_t serial,
                       struct wl_surface *surface)
{
}

static void
handle_keyboard_key (void *data,
                     struct wl_keyboard *wl_keyboard,
                     uint32_t serial,
                     uint32_t time,
                     uint32_t key,
                     uint32_t state)
{
  if (state != WL_KEYBOARD_KEY_STATE_PRESSED)
    return;

  fprintf(stderr, ":::: %s:%d %s() - \n", __FILE__, __LINE__, __func__);
  if (key == KEY_Q)
    exit (EXIT_SUCCESS);
  else if (key == KEY_R)
    reset_surface ();
  else if (key == KEY_1)
    toggle_fullscreen_surface (TRUE);
  else if (key == KEY_2)
    toggle_fullscreen_surface (FALSE);
}

static void
handle_keyboard_modifiers (void *data,
                           struct wl_keyboard *wl_keyboard,
                           uint32_t serial,
                           uint32_t mods_depressed,
                           uint32_t mods_latched,
                           uint32_t mods_locked,
                           uint32_t group)
{
}

static void
handle_keyboard_repeat_info (void *data,
                             struct wl_keyboard *wl_keyboard,
                             int32_t rate,
                             int32_t delay)
{
}

static const struct wl_keyboard_listener wl_keyboard_listener = {
  handle_keyboard_keymap,
  handle_keyboard_enter,
  handle_keyboard_leave,
  handle_keyboard_key,
  handle_keyboard_modifiers,
  handle_keyboard_repeat_info,
};

static void
handle_seat_capabilities (void           *data,
                          struct wl_seat *wl_seat,
                          uint32_t        capabilities)
{
  if (capabilities & WL_SEAT_CAPABILITY_KEYBOARD &&
      !keyboard)
    {
      keyboard = wl_seat_get_keyboard (wl_seat);
      wl_keyboard_add_listener (keyboard, &wl_keyboard_listener, NULL);
    }
  else if (keyboard)
    {
      wl_keyboard_release (keyboard);
    }
}

static const struct wl_seat_listener wl_seat_listener = {
    handle_seat_capabilities
};

static void
handle_registry_global (void               *data,
                        struct wl_registry *registry,
                        uint32_t            id,
                        const char         *interface,
                        uint32_t            version)
{
  if (strcmp (interface, "wl_compositor") == 0)
    {
      compositor = wl_registry_bind (registry, id, &wl_compositor_interface, 1);
    }
  else if (strcmp (interface, "xdg_wm_base") == 0)
    {
      xdg_wm_base = wl_registry_bind (registry, id,
                                      &xdg_wm_base_interface, 1);
      xdg_wm_base_add_listener (xdg_wm_base, &xdg_wm_base_listener, NULL);
    }
  else if (strcmp (interface, "wl_shm") == 0)
    {
      shm = wl_registry_bind (registry,
                              id, &wl_shm_interface, 1);
    }
  else if (strcmp (interface, "wl_seat") == 0 &&
           !seat)
    {
      seat = wl_registry_bind (registry,
                               id, &wl_seat_interface, 1);
      wl_seat_add_listener (seat, &wl_seat_listener, NULL);
    }
}

static void
handle_registry_global_remove (void               *data,
                               struct wl_registry *registry,
                               uint32_t            name)
{
}

static const struct wl_registry_listener registry_listener = {
  handle_registry_global,
  handle_registry_global_remove
};

static void
print_help (char **argv)
{
  fprintf (stdout, "Usage: %s [OPTIONS]\n", argv[0]);
  fprintf (stdout, "Options:\n");
  fprintf (stdout, "    --help                    - Show help.\n");
}

static void
init_surface (void)
{
  xdg_toplevel_set_title (xdg_toplevel, "gradient-test");
  wl_surface_commit (surface);

  wait_for_configure = TRUE;
}

static void
reset_surface (void)
{
  printf ("Resetting surface..\n");

  wl_surface_attach (surface, NULL, 0, 0);
  wl_surface_commit (surface);

  init_surface ();
}

int
main (int    argc,
      char **argv)
{
  if (argc >= 2)
    {
      if (strcmp (argv[1], "--help") == 0)
        {
          print_help (argv);
          return EXIT_SUCCESS;
        }
      else
        {
          print_help (argv);
          return EXIT_FAILURE;
        }
    }

  display = wl_display_connect (NULL);
  registry = wl_display_get_registry (display);
  wl_registry_add_listener (registry, &registry_listener, NULL);
  wl_display_roundtrip (display);

  if (!shm)
    {
      fprintf (stderr, "No wl_shm global\n");
      return EXIT_FAILURE;
    }

  if (!xdg_wm_base)
    {
      fprintf (stderr, "No xdg_wm_base global\n");
      return EXIT_FAILURE;
    }

  wl_display_roundtrip (display);

  surface = wl_compositor_create_surface (compositor);
  xdg_surface = xdg_wm_base_get_xdg_surface (xdg_wm_base, surface);
  xdg_surface_add_listener (xdg_surface, &xdg_surface_listener, NULL);
  xdg_toplevel = xdg_surface_get_toplevel (xdg_surface);
  xdg_toplevel_add_listener (xdg_toplevel, &xdg_toplevel_listener, NULL);

  init_surface ();

  fprintf (stdout,
           "Usage:\n"
           "\n"
           "Key bindings:\n"
           "   r      - reset surface\n"
           "   q      - quit\n");

  running = TRUE;
  while (running)
    {
      if (wl_display_dispatch (display) == -1)
        return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
